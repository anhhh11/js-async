const S = highland;
const d = debug('js-async');
function run() {
    //Stream from array
    const sa = S([1, 2, 3, 4]);
    sa.doto(function (v) {
        d('SA VAL', v);
    }).resume();
    //Stream from Nodejs Stream, Iterable, Iterator--> TODO
    //Stream from Promise
    const sp = S(Promise.resolve(1));
    sp.doto(function (v) {
        d('SP VAL', v)
    }).resume();
    //Stream from func generator
    const s1 = S(function (push, next) {
        var count = 0;
        var interval = setInterval(function () {
            count += 1;
            if (count == 5) {
                clearInterval(interval);
                next(S([1, 2, 3, 4]));
                // If call next() it will recall stream
                // If call next(stream) it will merge to next stream
                //push(null, S.nil);
                return;
            }
            const val = Math.random();
            if (val < 0.2) {
                push(val);
            } else {
                push(null, Math.random());
            }
        }, 200);
    });
    s1
        .errors(function (err) {
            d('S1 ERR', err);
        })
        .doto(function (val) {
            d('S1 VAL', val);
        })
        .done(function () {
            d('S1 DONE');
        });
    // Stream from jQuery
    S('mousemove', $('body'))
        .map(function (e) {
            return [e.pageX, e.pageY];
        })
        //.debounce(1000)
        .throttle(1000)
        .doto(function (pos) {
            d('SJ VAL', pos);
        })
        .resume();
}
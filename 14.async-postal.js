const d = debug('async-postal');
const bus = postal.channel('bus');
//== calc1()
bus.subscribe('calc1', function (data, env) {
  if (data.errorFlag) {
    return env.reply(new Error('Calc1'));
  }
  return env.reply(null, {result: 5});
});
//== calc2()
bus.subscribe('calc2', function (data, env) {
  return env.reply(null, {result: 5});
});
postal.subscribe({
  channel: 'bus',
  topic: '#',
  callback: function (d, e) {
    console.log('bus.#:', d, e);
  }
});
//postal.subscribe({
//  channel: 'postal.request-response',
//  topic: '#',
//  callback: function (d, e) {
//    console.log('postal.request-response.#:', d, e);
//  }
//});
function run() {
  const r1 = bus.request({topic: 'calc1', data: {errorFlag: false}});
  const r2 = bus.request({topic: 'calc2'});
  const ret = Promise.join(r1, r2, function (r1, r2) {
    return {result: r1.result + r2.result};
  });
  ret.tap(function (ret) {
    console.log(ret.result);
  });
}
/* Kết quả:
 bus.#: Object {errorFlag: false} Object {topic: "calc1", data: Object, headers: Object, channel: "bus", timeStamp: Wed Feb 17 2016 00:24:21 GMT+0700 (SE Asia Standard Time)}
 bus.#: undefined Object {topic: "calc2", data: undefined, headers: Object, channel: "bus", timeStamp: Wed Feb 17 2016 00:24:21 GMT+0700 (SE Asia Standard Time)}
 10
 */
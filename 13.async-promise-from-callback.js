// Cấu trúc: hàm riêng lẻ, sử dụng các hàm, xử lý lỗi, ngoại lệ(exception)
// Không cần viết lại các hàm dạng node callback
function calc1(errorFlg, cb) {
  setTimeout(function () {
    if
    (errorFlg) {
      //throw new Error('Calc 1') -> Không catch được
      return cb(new Error('Calc 1'));
    }
    return cb(null, 5);
  }, 1000);
}
function calc2(cb) {
  cb(null, 5)
}
function run() {
  const r1 = Promise.fromCallback(function(cb){
    return calc1(false,cb);
  });
  const r2 = Promise.fromCallback(calc2);
  r1.tap(function(r1){
    console.debug('debug value of r1:',r1);
  });
  const ret = Promise.join(r1,r2,function(r1,r2){
    return r1 + r2;
  });
  ret
    .then(function (ret) {
      console.info('RET', ret);
    })
    .catch(function (e) {
      console.info('ERROR', e);
    })
    .finally(function () {
      console.info('FINALLY', 'sync finally');
    });
}

/* Kết quả:
 ERROR Error: Calc1(…)
 FINALLY sync finally
 --> Đúng thứ tự + có stack trace
 */
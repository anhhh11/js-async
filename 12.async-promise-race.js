function serverX(name, distance, isDeath) {
  if (isDeath)
    return Promise
      .delay(distance)
      .thenThrow(new Error({name: name, distance: distance, state: 'death'}))
      .catch(function(err){
        console.log('server:',name);
      });
  return Promise
    .delay(distance)
    .thenReturn({name: name, distance: distance})
    .tap(function(){
      console.log('server:',name);
    });
}
function run() {
  const selectedServer =
    //race()
    //any()
    Promise
      .some([
        serverX('server1', 2000),
        serverX('server2', 500),
        serverX('server3', 300, true),
        serverX('server4', 1000),
        serverX('server5', 10000)
      ],2);
  selectedServer
    .catch(function (e) {
      console.log('error:', e);
    })
    .then(function (s) {
      console.log('selected server:', s);
    });
}

/* Kết quả, đợi 10s
 selected server: Object {name: "server3", distance: 300}
 */
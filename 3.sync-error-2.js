// Xử lý ngoại lệ 2
function calc1(errorFlg){
  if(errorFlg){
    throw new Error('Calc 1');
  }
  return 5;
}
function calc2(){
  return 5;
}
function run(){
  try {
    throw new Error('out error');
    const r1 = calc1(true);
    const r2 = calc2();
    const ret = r1 + r2;
    console.info('RET',ret);
  } catch(e) {
    console.info('ERROR',e);
  } finally  {
    console.info('FINALLY','sync finally');
  }
}
/* Kết quả:
 ERROR Error: out error
 Stack trace:
 run@http://localhost:3000/3.sync-error-2.js:13:1
 @http://localhost:3000/:15:5

 FINALLY sync finally
 */
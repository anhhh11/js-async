function run(){
	runGenerator(main);
}
function calc1(errorFlg) {
  return function(cb){
	  setTimeout(function(){
		if(errorFlg){
		  cb(new Error('Calc 1'));
		}
		cb(null,5);
	  }, 1000);
  }
}
function calc2(cb) {
  cb(null,6);
}
function *main(){
  try {
	  const calc1r = yield Promise.promisify(calc1(false))();
	  const calc2r = yield Promise.promisify(calc2)();
	  const ret = calc1r + calc2r;
	  console.log('RET',ret);
  } catch(e) {
	  console.info('ERROR',e.stack);
  } finally {
	  console.info('FINALLY','sync finally');
  }
}
function getJson(url){
	return new Promise(function(resolve,reject){
		$.getJSON(url,{},function(data){
			resolve(data);
		});
	});
}

function isPromise(o){
	return o.then;
}
function runGenerator(generator){
	var it = generator(),ret;
	(function iterate(err,value){
		if(err){
			it.throw(err);
			return;
		} else {
			ret = it.next(value);	
		}
		if(!ret.done){
			if(isPromise(ret.value)){
				ret.value.then(function(value){
					return iterate(null,value);
				}).catch(function(err){
					return iterate(err,null);
				});
			} else {
				setTimeout(function(){
					iterate(null,ret.value);	
				},0);
			}
		}
		return true;
	})(null,null);
}
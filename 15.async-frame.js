function calc1(errorFlg, cb) {
    setTimeout(function () {
        if
        (errorFlg) {
            //throw new Error('Calc 1') -> Không catch được
            return cb(new Error('Calc 1'));
        }
        return cb(null, 5);
    }, 1000);
}
function calc2(cb) {
    cb(null, 5)
}
function runNaive() {
    async.parallel({
        r1: function (cb) {
            return calc1(true, cb);
        },
        r2: calc2
    }, function (err, results) {
        if (err) {
            console.info('ERROR', err);
        } else {
            const ret = results.r1 + results.r2;
            console.info('RET', ret);
        }
        console.info('FINALLY', 'sync finally');
    });
}
function run() {
    async.auto({
        r1: async.apply(calc1, false),
        r2: calc2,
        ret: ['r1', 'r2', function (cb, r) {
            return cb(null, r.r1 + r.r2);
        }]
    }, null, function (err, r) {
        if (err) {
            console.info('ERROR', err);
        } else {
            const ret = r.ret;
            console.info('RET', ret);
        }
        console.info('FINALLY', 'sync finally');
    });
}
/* Kết quả
 ERROR Error: Calc 1 at http://localhost:3000/15.async-frame.js:6:23 --> Không có stack trace
 FINALLY sync finally
 */

/* Note các func hay dùng
 <name>Series - the same as <name> but runs only a single async operation at a time
 <name>Limit - the same as <name> but runs a maximum of limit async operations at a time
 -- Đa task
 series(tasks,[callback]) // Chạy tuần tự task này tới task khác kết quả lưu vào mảng.
 parallel(tasks,[callback]) // Chạy song song task này tới task khác kết quả lưu vào mảng.
 waterfall(tasks,[callback]) // Chạy tuần tự output của task trước là output của task sau. Hỗ trợ output nhiều param
 compose(fn1, fn2...) // Compose các hàm async -- Các hàm không phải async cần convert sang dạng async
 seq(fn1, fn2...) // Left to right version of compose
 applyEach(fns,args...,callback) ; applyEachSeries(fns,args...,callback)
 // nếu điều phối thực hiện quá phức tạp có thể dùng auto -- ý tưởng giống requirejs
 auto(Array<callback(err, result)>,[concurrency], callback(err, results))
 -- Functional
 map(arr, iteratee(item, callback), callback(err, result)) ; mapSeries ; mapLimit
 filter(arr, iteratee(item, callback), callback(results)) ; filterSeries ; filterLimit <> reject
 reduce(arr, initial, iteratee(memo, item, callback), callback(err, result))
 some ; someLimit
 every ; everyLimit
 concat ; concatSeries
 sortBy
 -- While/do/for
 whilst(test, fn, callback) ; doWhilst(fn, test, callback)
 until(test, fn, callback) ; doUntil(fn, test, callback)
 during(test, fn, callback) // Same as whilst but `test` is async  ; doDuring(fn, test, callback)
 forever(fn, [errback])
 times(n, iteratee, [callback]) ; timesSeries(n, iteratee, [callback]) ; timesLimit(n, limit, iteratee, [callback])
 retry({times: 5, interval: 0},task,[callback])
 iterator(tasks)
 -- Queue
 queue(worker, [concurrency]) / priorityQueue(worker, concurrency) -- Mỗi lần xử lý 1
 cargo(worker, [payload])   -- Mỗi lần xử lý N
 -- Utils
 apply(function, arguments..)
 nextTick(callback), setImmediate(callback)
 memoize(fn, [hasher])
 unmemoize(fn)
 ensureAsync(fn)
 constant(values...)
 asyncify(func)
 log(function, arguments)
 dir(function, arguments)
 noConflict()
 */









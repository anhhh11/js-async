// Async về cơ bản liên quan đến delay, phân chia task cho nguồn khác xử lý
function calc1(errorFlg,cb) {
  setTimeout(function(){
    if(errorFlg){
      cb(new Error('Calc 1'));
    }
    cb(null,5);
  }, 1000);
}
function calc2(cb) {
  cb(null,5)
}
function run() {
  try {
    calc1(false, function (err1, r1) {
      if (err1) throw err1;
      calc2(function (err2, r2) {
        if (err2) throw err2;
        const ret = r1 + r2;
        console.info('RET', ret);
      })
    });
  } catch(e) {
    console.info('ERROR',e);
  } finally  {
    console.info('FINALLY','sync finally');
  }
}

/* Kết quả:
 FINALLY sync finally --> Bị ngược
 RET 10
 */
function calc1(errorFlg, cb) {
  setTimeout(function () {
    if (errorFlg) {
      //throw new Error('Calc 1') -> Không catch được
      return cb(new Error('Calc 1'));
    }
    return cb(null, 5);
  }, 1000);
}
function calc2(cb) {
  cb(null, 5)
}
function run() {
  try {
    calc1(false, function (err1, r1) {
      if (err1) throw err1;
      calc2(function (err2, r2) {
        if (err2) throw err2;
        const ret = r1 + r2;
        console.info('RET', ret);
        finallyBlock();
      })
    });
  } catch (e) {
    console.info('ERROR', e);
    finallyBlock();
  }
 const finallyBlock = function(){
    console.info('FINALLY', 'sync finally');
  }
}

/* Kết quả:
 RET 10
 FINALLY sync finally
 */
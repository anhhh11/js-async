const S = highland;
const d = debug('js-async');
function calc1(errorFlg, cb) {
    setTimeout(function () {
        if
        (errorFlg) {
            //throw new Error('Calc 1') -> Không catch được
            return cb(new Error('Calc 1'));
        }
        return cb(null, 5);
    }, 1000);
}
function calc2(cb) {
    cb(null, 5)
}
function run() {
    //console.debug('debug value of r1:',r1);
    const r1 = S.wrapCallback(S.partial(calc1, false))();
    const r2 = S.wrapCallback(calc2)();
    const ret =
        r1.zip(r2) // Combine 2 -> 1 stream
            .map(function (rs) {
                //throw new Error('Some error');
                return rs[0] + rs[1];
            }) // Transform 1 stream -> new stream
            .doto(function (ret) {
                d('RET', ret);
            }) // Foreach item in stream print & re-emit to new stream
            .errors(function (err, push) {
                d('ERROR', err);
            }) // Extract error apply to handler return to new stream without error
            .done(function () {
                d('FINALLY', 'sync finally');
            }); // Call when stream ended
}
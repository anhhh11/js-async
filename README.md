# Cách cách điều phối bất đồng bộ (Control async)
1,2,3. Review về đồng bộ (synchronous) gọi hàm, bắt lỗi
4. Event loop trong JS với setTimeout
5,6,7,8,9. Asynchronous, bắt lỗi với async

# Danh sách các công cụ điều phối bất đồng bộ
1. Callback (CPS - Composing Async Process) -> Done
2. Promise (Bluebird) -> Done
3. PubSub (PostalJS) -> Done
4. Frame (async)
5. Node EventStream
6. Stream (RxJS)
7. Generator (Native)
8. Finite State Machine(FSM)
9. goChannel (js-csp)
10. Async/await

# 1. Callback
Ưu điểm:
- Dễ hiểu
Nhược điểm:
- Không scale tốt. VD: 4,5 hàm async lồng nhau @@.
- Việc kết hợp hàm(compose) khó khăn.
- Lắm boilerplate(code lặp đi lặp lại) như if(error) throw Error...
- Thiếu một số tính năng cần như cache lại dữ liệu hàm.

# 2. Promise
Ưu điểm:
- Dễ hiểu không khác mấy so với callback truyền thống.
- Compose các hàm async dễ dàng.
- Scale tốt.
- Có cache dữ liệu.
Nhược điểm:
- Có thể chậm hơn so với cách callback nhưng không đáng kể (với trường hợp số promise chạy song song là 10000 thì
thời gian phản hồi là ~350ms).
- Do có cache dữ liệu -> Promise chỉ thích hợp với trường hợp get full response
thay vì incremental processing.

# 3. PubSub
Ưu điểm:
- Mở rộng dễ dàng.
- Có thể tận dụng promise để compose các channel trong trường hợp dùng req-res pattern.
Nhược điểm:
- Nhiều boilerplate.
- Cần tính toán thiết kế kiến trúc thông tin hệ thống ngay từ đầu.

# 4. Khung

# 5. Stream
Ưu điểm:
- Rất mạnh trong xử lý event liên quan tới thời gian, số lần, thao tác UI...
Nhược điểm:
- ???

# Phần tiếp theo
- Giới thiệu về functional programming
- Giới thiệu về monad....

# Danh sách nguồn tham khảo
[http://eamodeorubio.github.io/tamingasync] CPS, Promise
[http://robotlolita.me/2013/06/28/promises-considered-harmful.html] Promise/A+ disadvantage
[http://blog.carbonfive.com/2013/10/27/the-javascript-event-loop-explained/] Event loop
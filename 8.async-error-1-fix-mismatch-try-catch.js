function calc1(errorFlg, cb) {
  setTimeout(function () {
    if
    (errorFlg) {
      //throw new Error('Calc 1') -> Không catch được
      return cb(new Error('Calc 1'));
    }
    return cb(null, 5);
  }, 1000);
}
function calc2(cb) {
  cb(null, 5)
}
function run() {
  var error;
  calc1(true, function (err1, r1) {
    if (err1) return catchError(err1);
    calc2(function (err2, r2) {
      if (err2) return catchError(err2);
      const ret = r1 + r2;
      console.info('RET', ret);
      finallyBlock();
    })
  });
  const catchError = function (error) {
    if (error) {
      console.info('ERROR', error);
      finallyBlock();
    }
  };
  const finallyBlock = function () {
    console.info('FINALLY', 'sync finally');
  };
}

/* Kết quả:
 ERROR Error: Calc 1
 Stack trace:
 calc1/<@http://localhost:3000/8.async-error-1-fix-mismatch-try-catch.js:6:17

 FINALLY sync finally
 ---> Stack trace của hàm gọi nó bị mất
 */
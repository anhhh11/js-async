function f1() {
  console.log('f1');
}
function f2() {
  console.log('f2');
}
function f3() {
  console.log('f3');
}
function normalExample() {
  console.log('Normal Example');
  f1();
  f2();
}
// Với 1 của sổ (Tab trên browser)
// Có 2 luồng thực hiện và javascript luân phiên chuyển 2 luồng này
// Luồng 1: thực hiện lệnh tuần tự
// Luồng 2: nhận và gửi event cho luồng 2
// Luồng 1 thực hiện xong thì JS Engine chuyển sang Luồng 2 quét và xử lý
// Việc click chuột, gửi form, click/tap button sinh ra các event gửi vào luồng 2 xử lý.
function delayExample() {
  console.log('Delay Example');
  setTimeout(f1, 0);
  f2();
}
function delayExample2() {
  console.log('Delay Example 2');
  setTimeout(f1, 0);
  f2();
  setTimeout(f3, 0);
}
function run() {
  normalExample();
  delayExample();
  delayExample2();
}

/* Kết quả:
 //Ở luồng 1
 Normal Example
 f1
 f2
 Delay Example
 //Từ luồng 1, gửi event dạng {event: 'Timeout', handler: f1 <0x1234>} vào luồng 2
 f2 // Thực hiện ở luồng 1
 f1 // Luồng 1 thực hiện xong, chuyển sang luồng 2 có 1 event là 'Timeout', event này được gửi(dispatch) cho luồng 1 xử lý
 // Luồng 1 xử lý xong, engine chuyển sang luồng 2, luồng 2 hết event, chuyển qua luồng 1
 // Luồng 1 xử lý xong -> Tab sleep
 */

/* Kết quả uncomment delayExample2()
 Trong ví dụ delayExample2()
 Normal Example
 f1
 f2
 Delay Example
 f2
 Delay Example 2
 f2
 f1
 f1
 f3
 --> Thứ tự thực hiện bằng setTimeout dựa trên thứ tự chạy của nhiều hàm
 --> Sử dụng quá nhiều setTimeout làm phức tạp trong việc xác định thứ tự chạy.
 Ngoài ra sử dụng setTimeout với yếu tố thời gian 200ms, 1s..... sẽ càng phức tạp hơn
 --> Thường không nên sử dụng setTimeout trong điều khiển async
 */
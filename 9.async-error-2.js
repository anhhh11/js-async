function calc1(errorFlg, cb) {
  setTimeout(function () {
    if
    (errorFlg) {
      //throw new Error('Calc 1') -> Không catch được
      return cb(new Error('Calc 1'));
    }
    return cb(null, 5);
  }, 1000);
}
function calc2(cb) {
  cb(null, 5)
}
function run() {
  var error;
  calc1(true, function (err1, r1) {
    if (err1) return catchError(err1);
    calc2(function (err2, r2) {
      if (err2) return catchError(err2);
      try {
        const ret = r1 + r2;
        console.info('RET', ret);
      } catch (e){
        catchError(e);
      } finally {
        finallyBlock();
      }
    })
  });
  const catchError = function (error) {
    if (error) {
      console.info('ERROR', error);
      finallyBlock();
    }
  };
  const finallyBlock = function () {
    console.info('FINALLY', 'sync finally');
  };
}

/* Kết quả:
 ERROR Error: Calc 1
 Stack trace:
 calc1/<@http://localhost:3000/9.async-error-2.js:6:17

 FINALLY sync finally
 ---> Tới đây đã khá phức tạp do lai giữa 2 cách: 1 cách `lách` để hỗ trợ bắt lỗi delay + 1 cách sử dụng try-catch-finally thông thường
 ---> Thực tế để tránh phức tạp + code gọn + debug dễ hơn sẽ ít dùng cách callback trong trường hợp >= 3 lớp lồng nhau (callback hell) thay vào đó
 dùng công cụ khác như Promise hay Stream
 */
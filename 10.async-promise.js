// Cấu trúc: hàm riêng lẻ, sử dụng các hàm, xử lý lỗi, ngoại lệ(exception)
function calc1(errorFlg) {
  if (errorFlg) {
    return Promise.reject(new Error('Calc1'));
  }
  return Promise.delay(1000).then(function () {
    return 5;
  });
}
function calc2() {
  return 5;
}
function run() {
  const r1 = calc1();
  const r2 = calc2();
  //const ret = Promise
  //      .all([r1, r2])
  //      .then(function (rs) {
  //        throw new Error('out error');
  //        return rs[0] + rs[1];
  //      });
  //OR
  //const ret = Promise.resolve([r1, r2]).spread(function (r1, r2) {
  //  return r1 + r2;
  //});
  r1.tap(function(r1){
    console.debug('debug value of r1:',r1);
  });
  const ret = Promise.join(r1,r2,function(r1,r2){
    return r1 + r2;
  });
  ret
    .then(function (ret) {
      console.info('RET', ret);
    })
    .catch(Error,function (e) {
      console.info('ERROR', e);
    })
    .finally(function () {
      console.info('FINALLY', 'sync finally');
    });
}

/* Kết quả:
 ERROR Error: Calc1(…)
 FINALLY sync finally
 --> Đúng thứ tự + có stack trace
 */
// Xử lý ngoại lệ 1
function calc1(errorFlg){
  if(errorFlg){
    throw new Error('Calc 1');
  }
  return 5;
}
function calc2(){
  return 5;
}
function run(){
  try {
    const r1 = calc1(true);
    const r2 = calc2();
    const ret = r1 + r2;
    console.info('RET',ret);
  } catch(e) {
    console.info('ERROR',e);
  } finally  {
    console.info('FINALLY','sync finally');
  }
}
/* Kết quả:
 ERROR Error: Calc 1
 Stack trace:
 calc1@http://localhost:3000/2.sync-error.js:4:1
 run@http://localhost:3000/2.sync-error.js:13:16
 @http://localhost:3000/:14:5

 FINALLY sync finally
 */
const S = highland;
const d = debug('js-async');
function run() {
    S(function (push, next) {
        setInterval(function () {
            return push(null, Math.random());
        }, 200);
    })
    //.append(5)
    //.batch(2)
        .batchWithTimeOrCount(400, 10)
        .doto(async.apply(d, 'VAL'))
    //.resume();
    S([1, 2, 3, 4, null, NaN])
    //.toArray(async.apply(d,'VAL'))
        .compact()
        .drop(1)
        .filter(function (x) {
            return x > 2
        })
        .collect()
        .doto(async.apply(d, 'VAL'))
        .resume();
    const _names = S(['john', 'bob', 'bob', 'henry']);
    _names
        .find(function (x) {
            return x == 'bob'
        })
        .doto(async.apply(d, 'VAL FOUND'))
    //.resume();
    const _people = S([{name: 'john', age: 18}, {name: 'bob', age: 18}, {name: 'henry', age: 19}]);
    const _john = _people
        .observe()
        .findWhere({name: 'john'})
        .doto(async.apply(d, 'VAL WHERE'));
    const _age18Group = _people
        .fork()
        .group(function (p) {
            return p.age;
        })
        .doto(async.apply(d, 'VAL GROUP'));
    _john.done(async.apply(d, 'VAL WHERE DONE'));
    _age18Group.done(async.apply(d, 'VAL GROUP DONE'));
    S('mousemove',$('body'))
        .map(function (e) {
            return [e.pageX, e.pageY];
        })
        .latest()
        .doto(async.apply(d, 'MOUSEPOS LATEST'))
        .resume();
}
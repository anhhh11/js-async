/**
 * Created by anhhh11 on 2/17/2016.
 */
function defer() {
  var resolve, reject;
  var promise = new Promise(function() {
    resolve = arguments[0];
    reject = arguments[1];
  });
  return {
    resolve: resolve,
    reject: reject,
    promise: promise
  };
}
// We need to tell postal how to get a deferred instance
postal.configuration.promise.createDeferred = function() {
  return defer();
};
// We need to tell postal how to get a "public-facing"/safe promise instance
postal.configuration.promise.getPromise = function(dfd) {
  return dfd.promise;
};

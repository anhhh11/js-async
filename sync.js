// Cấu trúc: hàm riêng lẻ, sử dụng các hàm, xử lý lỗi, ngoại lệ(exception)
function calc1(errorFlg){
  if(errorFlg){
    throw new Error('Calc 1');
  }
  return 5;
}
function calc2(){
  return 5;
}
function sync(){
  try {
    const r1 = calc1();
    const r2 = calc2();
    const ret = r1 + r2;
    console.info('RET',ret);
  } catch(e) {
    console.info('ERROR',e);
  } finally  {
    console.info('FINALLY','sync finally');
  }
}
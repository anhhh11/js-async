function calc() {
  return Promise.resolve([1,2,3,4,5]);
}
function run() {
  const r = calc();
  const ret =
    r
      .map(function(x){return x+1})
      .reduce(function(acc,item){
        return acc + item;
      },0);
  ret
    .tap(function(ret){
      console.log('ret:',ret);
    })
}

/* Kết quả:
 ret: 20
 */